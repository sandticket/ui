FROM beevelop/ionic:latest
RUN mkdir -p /src/project
WORKDIR /src/project
COPY . .
RUN npm install -g @angular/cli
RUN npm install --save-dev @angular-devkit/build-angular
RUN npm install -g ionic
EXPOSE 8100
EXPOSE 8200
CMD ionic serve --host 0.0.0.0 --address=0.0.0.0 --port=$PORT

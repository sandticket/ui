/// <reference types="Cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8100/')
  })
  it("test ticket creation", () =>{
    

    cy.get("#usernameinput>input")
      .type("user1").should('have.value','user1')
    cy.get("#passwordinput>input")
      .type("user")
    cy.get("#loginformsubmit")
      .click()
    cy.get("#ticketcreationbutton")
      .click()
    cy.get("#tickettitle>input")
      .type("testtitle")
    cy.get("#ticketdesc>textarea")
      .type("desc test ")
    cy.get("#ticketsubmit")
      .click()
    cy.get("#ticketsearchbar input")
      .type("testtitle")
  })
})
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'tickets', loadChildren: './pages/tickets/tickets.module#TicketsPageModule' },
  { path: 'ticket/:id', loadChildren: './pages/ticket-details/ticket-details.module#TicketDetailsPageModule' },
  { path: 'add-ticket', loadChildren: './pages/add-ticket/add-ticket.module#AddTicketPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

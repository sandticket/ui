import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.page.html',
  styleUrls: ['./add-ticket.page.scss'],
})
export class AddTicketPage implements OnInit {

  title = '';
  dueDate = '';
  description = '';
  
  constructor(private activatedRoute: ActivatedRoute, private ticketService: TicketService) { }

  ngOnInit() {
  }

  createTicket(){

    this.ticketService.createTicket(this.title, this.dueDate, this.description);
  }
}

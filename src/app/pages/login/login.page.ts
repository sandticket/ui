import { Component, OnInit } from '@angular/core';
import {Observable, from} from 'rxjs';
import {LoginService} from './../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username = '';
  password = '';
  results: Observable<any>;

  constructor(private loginService : LoginService) { }

  ngOnInit() {
   
  }

  login()
{
  console.log('username page.ts', this.username);
  console.log('password page.ts', this.password);


  this.results = this.loginService.login(this.username, this.password)
  console.log('results page.ts', this.results);
  
}
}

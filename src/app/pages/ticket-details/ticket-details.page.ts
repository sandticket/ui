import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-ticket-details',
  templateUrl: './ticket-details.page.html',
  styleUrls: ['./ticket-details.page.scss'],
})
export class TicketDetailsPage implements OnInit {
information = null;
  constructor(private activatedRoute: ActivatedRoute, private ticketService: TicketService) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    
    this.ticketService.getDetails(id).subscribe(result =>{
      this.information = result;
    })
  }
  
}

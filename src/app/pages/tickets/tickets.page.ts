import {TicketService} from './../../services/ticket.service';
import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.page.html',
  styleUrls: ['./tickets.page.scss'],
})
export class TicketsPage implements OnInit {

  results: Observable<any>;
  searchTerm = '';

  constructor(private ticketService : TicketService) { }

  ngOnInit() {
      this.results= this.ticketService.getAllTickets();
  }

  searchChanged(){
    console.log("search term", this.searchTerm);
    
    if(this.searchTerm == ''){
    console.log("empty term");

      this.results = this.ticketService.getAllTickets();

    } else {
    console.log("not empty term");

    this.results = this.ticketService.searchData(this.searchTerm)

     }

    }
    onClear(){
      this.results = this.ticketService.getAllTickets();
     console.log('cleared');
     
    }

    getAllTickets(){

      this.results= this.ticketService.getAllTickets();
      
    }

  }


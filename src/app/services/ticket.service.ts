import {HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from  "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class TicketService {

  url = 'http://localhost:8102/';
  constructor(private http: HttpClient, private router: Router) { }

  getAllTickets (): Observable<any>{   
    
     return this.http.get(`${this.url}tasks/`)
     .pipe(
       map(results => {
         console.log('tasks', results);
         return results;
       })
     );
   }

  searchData (title: string): Observable<any>{ 
   
    return this.http.get(`${this.url}tasks?title=${title}`)
    .pipe(
      map(results => {
        console.log('RAW', results);
        return results;
      })
    );
  }
  getDetails(id): Observable<any>{
    console.log('query for id : ', id);
    
    return this.http.get(`${this.url}tasks?taskId=${id}`);
  }
  createTicket(title: string, dueDate : string, description: string){
    //post method for ticket
    
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );
  
      let postData = {
        //data here
              "title": `${title}`,
              "dueDate": `${dueDate}`,
              "description": `${description}`,
              "lastUpdateDate": "",
              "status": "NOT_CLOSED",
              "creator": null,
              "creationDate": ""
      }
      console.log('postData', postData)
      this.http.post(`${this.url}tasks/create`, postData, { headers : {
        'Content-Type' : 'application/json'
      }
    })
        .subscribe(data => {
          console.log(data['_body']);
         }, error => {
          console.log(error);
        });
      this.router.navigateByUrl("tickets");

  }
}
